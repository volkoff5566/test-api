In order to run the test project locally use:

   `mvn clean test`

For running the test project remotely inside CI runner you should trigger the pipeline manually.

Pipeline will be triggered automatically after pushing changes to remote repository.

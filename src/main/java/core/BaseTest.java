package core;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.config.EncoderConfig.encoderConfig;
import static io.restassured.config.RestAssuredConfig.config;

public class BaseTest {

    protected static RequestSpecification requestSpec;
    protected static Map<String, Object> jsonBodyAsMap = new HashMap<>();
    private static final String URI = "https://reqres.in/api";

    protected static void setRequestSpecification() {
        requestSpec = new RequestSpecBuilder()
                .setBaseUri(URI)
                .setConfig(config().encoderConfig(encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false)))
                .build();
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        RestAssured.useRelaxedHTTPSValidation();
    }

}

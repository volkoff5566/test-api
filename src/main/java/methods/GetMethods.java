package methods;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public class GetMethods {

    public Response get(RequestSpecification requestSpec, String resources, int statusCode) {
        return  given().
                spec(requestSpec).
                        when()
                .get(resources).
                        then()
                .contentType(JSON)
                .statusCode(statusCode)
                .extract().response();
    }
}

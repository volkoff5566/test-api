package methods;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class DeleteMethods {

    public Response delete(RequestSpecification requestSpec, String resources, int statusCode) {
        return  given().
                spec(requestSpec).
                        when()
                .delete(resources).
                        then()
                .statusCode(statusCode)
                .extract().response();
    }
}

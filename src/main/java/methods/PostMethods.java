package methods;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public class PostMethods {

    public Response postWithBody(RequestSpecification requestSpec, String resources, Object body, int statusCode) {
        return  given().
                spec(requestSpec)
                .body(body)
                .contentType(JSON).
                        when()
                .post(resources).
                        then()
                .contentType(JSON)
                .statusCode(statusCode)
                .extract().response();
    }
}

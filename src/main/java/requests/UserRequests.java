package requests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import methods.DeleteMethods;
import methods.GetMethods;
import methods.PatchMethods;
import methods.PostMethods;

import java.util.Map;

public class UserRequests {

    private RequestSpecification requestSpec;
    private PostMethods postMethods = new PostMethods();
    private DeleteMethods deleteMethods = new DeleteMethods();
    private GetMethods getMethods = new GetMethods();
    private PatchMethods patchMethods = new PatchMethods();

    public UserRequests(RequestSpecification requestSpec) {
        this.requestSpec = requestSpec;
    }

    public Response createUser(Map<String, Object> body, int statusCode) {
        String endpoint = "/users";
        return postMethods.postWithBody(requestSpec, endpoint, body, statusCode);
    }

    public Response deleteUser(String userId, int statusCode) {
        String endpoint = "/users/" + userId;
        return deleteMethods.delete(requestSpec, endpoint, statusCode);
    }

    public Response getUser(String userId, int statusCode) {
        String endpoint = "/users/" + userId;
        return getMethods.get(requestSpec, endpoint, statusCode);
    }

    public Response updateUser(String userId, Map<String, Object> body, int statusCode) {
        String endpoint = "/users/" + userId;
        return patchMethods.patch(requestSpec, endpoint, body, 200);
    }


}

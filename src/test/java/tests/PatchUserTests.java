package tests;

import core.BaseTest;
import io.restassured.response.Response;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import requests.UserRequests;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.testng.Assert.assertEquals;

public class PatchUserTests extends BaseTest {
    private static UserRequests userRequests;
    private static String userId;
    private static String username = "James Bond";
    private static String job = "QA Engineer";

    @BeforeClass
    public static void setUp() {
        setRequestSpecification();
        userRequests = new UserRequests(requestSpec);

        jsonBodyAsMap.put("name", username);
        jsonBodyAsMap.put("job", job);
        Response response = userRequests.createUser(jsonBodyAsMap, 201)
                .then().assertThat()
                .body(matchesJsonSchemaInClasspath("createUserResponseSchema.json"))
                .extract().response();
        userId = response.jsonPath().getString("id");
        jsonBodyAsMap.clear();
    }

    @Test
    public void postUser() {
        String newUsername = "Viktor Pavlik";
        String newJob = "Super-start";
        jsonBodyAsMap.put("name", newUsername);
        jsonBodyAsMap.put("job", newJob);

        Response response = userRequests.updateUser(userId, jsonBodyAsMap, 200)
                .then().assertThat()
                .body(matchesJsonSchemaInClasspath("updateUserResponseSchema.json"))
                .extract().response();

        assertEquals(response.jsonPath().getString("name"), newUsername);
        assertEquals(response.jsonPath().getString("job"), newJob);
    }

    @AfterClass
    public void cleanUp() {
        userRequests.deleteUser(userId, 204);
        jsonBodyAsMap.clear();
    }
}

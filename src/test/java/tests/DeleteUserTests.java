package tests;

import core.BaseTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import requests.UserRequests;


public class DeleteUserTests extends BaseTest {
    private static UserRequests userRequests;
    private static String userId = "12";

    @BeforeClass
    public static void setUp() {
        setRequestSpecification();
        userRequests = new UserRequests(requestSpec);
    }

    @Test
    public void deleteUser() {
        userRequests.deleteUser(userId, 204);
    }

}

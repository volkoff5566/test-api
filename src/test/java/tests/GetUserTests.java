package tests;

import core.BaseTest;
import io.restassured.response.Response;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import requests.UserRequests;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.testng.Assert.assertEquals;


public class GetUserTests extends BaseTest {
    private static UserRequests userRequests;
    private String userId = "2";
    private String email = "janet.weaver@reqres.in";


    @BeforeClass
    public static void setUp() {
        setRequestSpecification();
        userRequests = new UserRequests(requestSpec);
    }

    @Test
    public void getUser() {
        Response response = userRequests.getUser(userId, 200)
                .then().assertThat()
                .body(matchesJsonSchemaInClasspath("getUserResponseSchema.json"))
                .extract().response();

        assertEquals(response.jsonPath().getString("data.email"), email);
    }
}

package tests;

import core.BaseTest;
import io.restassured.response.Response;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import requests.UserRequests;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.testng.Assert.assertEquals;


public class CreateUserTests extends BaseTest {
    private static UserRequests userRequests;
    private static String userId;
    private String username = "James Bond";
    private String job = "QA Engineer";

    @BeforeClass
    public static void setUp() {
        setRequestSpecification();
        userRequests = new UserRequests(requestSpec);
    }

    @Test
    public void postUser() {
        jsonBodyAsMap.put("name", username);
        jsonBodyAsMap.put("job", job);
        Response response = userRequests.createUser(jsonBodyAsMap, 201)
                .then().assertThat()
                .body(matchesJsonSchemaInClasspath("createUserResponseSchema.json"))
                .extract().response();

        assertEquals(response.jsonPath().getString("name"), username);
        assertEquals(response.jsonPath().getString("job"), job);

        userId = response.jsonPath().getString("id");
    }

    @AfterClass
    public void cleanUp() {
        userRequests.deleteUser(userId, 204);
        jsonBodyAsMap.clear();
    }

}
